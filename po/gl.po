# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Xosé, 2017
# Daniel Muñiz Fontoira <dani@damufo.eu>, 2019
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: xfce4-statusnotifier-plugin\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-07-28 16:38+0200\n"
"PO-Revision-Date: 2017-07-23 15:49+0000\n"
"Last-Translator: Daniel Muñiz Fontoira <dani@damufo.eu>, 2019\n"
"Language-Team: Galician (https://www.transifex.com/xfce/teams/16840/gl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: gl\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/sn-dialog.c:368
msgid "Clear"
msgstr "Limpar"

#: ../panel-plugin/sn-dialog.c:369
msgid "Are you sure you want to clear the list of known items?"
msgstr "Desexa realmente limpar a lista de elementos coñecidos?"

#: ../panel-plugin/sn-dialog.glade.h:1
msgid "Status Notifier Items"
msgstr "Elementos do notificador de estado"

#: ../panel-plugin/sn-dialog.glade.h:2
msgid "_Maximum icon size (px):"
msgstr "Tamaño _máximo da icona (px):"

#: ../panel-plugin/sn-dialog.glade.h:3
msgid "Arrange items in a single row"
msgstr "Arrombar os elementos nunha única fila"

#: ../panel-plugin/sn-dialog.glade.h:4
msgid "If enabled, ensure that the items are laid out in a single row"
msgstr ""
"Se é activado, asegúrese de que os elementos se dispoñen nunha única fila"

#: ../panel-plugin/sn-dialog.glade.h:5
msgid "Square icons"
msgstr "Iconas cadradas"

#: ../panel-plugin/sn-dialog.glade.h:6
msgid "Item buttons will take a square when it's possible"
msgstr "Os botóns dos elementos ocuparán un cadrado cando sexa posíbel."

#: ../panel-plugin/sn-dialog.glade.h:7
msgid "Symbolic icons"
msgstr "Iconas simbólicas"

#: ../panel-plugin/sn-dialog.glade.h:8
msgid "Load symbolic icons if available"
msgstr "Cargar iconas simbólicas se están dispoñíbeis"

#: ../panel-plugin/sn-dialog.glade.h:9
msgid "Appearance"
msgstr "Aparencia"

#: ../panel-plugin/sn-dialog.glade.h:10
msgid "Menu is primary action"
msgstr "O menú é a acción primaria"

#: ../panel-plugin/sn-dialog.glade.h:11
msgid "Left click will always display the menu for item"
msgstr "O botón esquerdo amosará sempre o menú do elemento"

#: ../panel-plugin/sn-dialog.glade.h:12
msgid "Behavior"
msgstr "Comportamento"

#: ../panel-plugin/sn-dialog.glade.h:13
msgid "Hide items by default"
msgstr "Agochar os elementos de xeito predeterminado"

#: ../panel-plugin/sn-dialog.glade.h:14
msgid "When enabled, all new items will be marked as \"Hidden\""
msgstr "Se é activado, todos os novos elementos marcaranse como «agochados»"

#: ../panel-plugin/sn-dialog.glade.h:15
msgid "Item"
msgstr "Elemento"

#: ../panel-plugin/sn-dialog.glade.h:16
msgid "Hidden"
msgstr "Agochado"

#: ../panel-plugin/sn-dialog.glade.h:17
msgid "Move the selected item one row up"
msgstr "Subir o elemento seleccionado unha fila"

#: ../panel-plugin/sn-dialog.glade.h:18
msgid "Move the selected item one row down"
msgstr "Baixar o elemento seleccionado unha fila"

#: ../panel-plugin/sn-dialog.glade.h:19
msgid "C_lear Known Items"
msgstr "_Limpar os elementos coñecidos"

#: ../panel-plugin/sn-dialog.glade.h:20
msgid "Resets the list of items and their visibility settings"
msgstr "Restaura a lista de elementos e as súas opcións de visibilidade"

#: ../panel-plugin/sn-dialog.glade.h:21
msgid "Known Items"
msgstr "Elementos coñecidos"

#: ../panel-plugin/sn-plugin.c:185
#: ../panel-plugin/statusnotifier.desktop.in.h:2
msgid ""
"Provides a panel area for status notifier items (application indicators)"
msgstr ""
"Fornece unha zona do panel para elementos de notificación de estado "
"(indicadores de aplicacións)"

#: ../panel-plugin/statusnotifier.desktop.in.h:1
msgid "Status Notifier Plugin"
msgstr "Engadido de notificador de estado"
